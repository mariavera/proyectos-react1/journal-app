import React from 'react'

export const JournalEntry = () => {
    return (
        <div className="journal__entry pointer">
            <div className="journal__entry-picture"
            style={{
                backgroundSize:'Cover',
                backgroundImage:'url(https://static.wikia.nocookie.net/3e8f137d-980e-499e-b90b-f83394e708bd)'

            }}
            >

            </div>

            <div className="journal__entry-body">
                <p className="journal__entry-title">
                    Hange Zoe
                </p>

                <p className="journal__entry-content">
                If there's something you don't understand, learn to understand it. 
                </p>

            </div>
            <div className="journal__entry-date-box">
                <span>Monday</span>
                <h4>28</h4>

            </div>
            
        </div>
    )
}
