import React from 'react';
import {useDispatch} from 'react-redux';
import { Link } from 'react-router-dom';
import {  startGoogleLogin, startLoginEmailPassword } from '../../actions/auth';
import { useForm } from '../../hooks/useForm';

export const LoginScreen = () => {
    const dispatch = useDispatch();

    const [ formValues, handleInputChange] = useForm({
        email:'nando@gmail.com',
        password:'123456'
    })
    const {email, password} =formValues;
    const handleLogin = (e) =>{
        e.preventDefault();
        dispatch( startLoginEmailPassword(email, password))
    }

    const handleLoginGoogle = ()=>{
        dispatch( startGoogleLogin())

    }
    return (
        <>
            <h3 className="auth__title mb-5">Login</h3>

            <form>
                <input
                    type="text"
                    placeholder="email"
                    name="email"
                    className="auth__input"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChange}


                />
                <input
                    type="password"
                    placeholder="******"
                    name="password"
                    className="auth__input"
                    value={password}
                    onChange={handleInputChange}

                />
                <button 
                className="btn btn-block btn-primary" 
                type="submit"
                onClick={handleLogin}>
                    Login
            </button>
                <hr />

                <div className="auth__social-networks">
                    <p>Login with social networks</p>
                    <div
                        className="google-btn"
                        onClick={handleLoginGoogle}
                    >
                        <div className="google-icon-wrapper">
                            <img className="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="google button" />
                        </div>
                        <p className="btn-text">
                            <b>Sign in with google</b>
                        </p>
                    </div>
                </div>
                <Link className="link" to= "/auth/register">
                    Create new account
                </Link>
            </form>
        </>
    )
}
