import React from 'react'
import { Link } from 'react-router-dom'

export const RegisterScreen = () => {
    return (
        <>
            <h3 className="auth__title mb-5">Register</h3>

<form>
    <input
        type="text"
        placeholder="email"
        name="email"
        className="auth__input"
        autoComplete="off"


    />

<input
        type="text"
        placeholder="name"
        name="name"
        className="auth__input"
        autoComplete="off"


    />
    <input
        type="password"
        placeholder="******"
        name="password"
        className="auth__input"

    />

<input
        type="password"
        placeholder="Confirm password"
        name="confirm"
        className="auth__input"

    />
    <button 
    className="btn btn-block btn-primary mb-5" 
    type="submit">
        Register
</button>

   
    <Link className="link" to= "/auth/login">
        Already registered? 
    </Link>
</form>
        </>
    )
}
