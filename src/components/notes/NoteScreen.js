import React from 'react'
import { NoteAppBar } from './NoteAppBar'

export const NoteScreen = () => {
    return (
        <div className="notes__main-content">
            <NoteAppBar/>
            <div className="notes__content">

                <input
                type="text"
                placeholder="Some awesome title"
                className="notes__title-input"
                autoComplete="off"
                />
                <textarea
                placeholder="What happened today?"
                className="notes__textarea"></textarea>

                <div className="notes__images">
                    <img
                    src="https://www.lifeder.com/wp-content/uploads/2018/04/levi.jpeg"
                    alt="imagen"
                ></img>

                </div>
            </div>
        </div>
    )
}
