import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCS3uJQ3C6AZXOftz4dF429IWbJQAqTK4U",
    authDomain: "react-app-cursos-837f9.firebaseapp.com",
    projectId: "react-app-cursos-837f9",
    storageBucket: "react-app-cursos-837f9.appspot.com",
    messagingSenderId: "713906327803",
    appId: "1:713906327803:web:240762b3f39d2f5482e05e"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();
  const googleAuthProv = new firebase.auth.GoogleAuthProvider();

  export {
      db,
      googleAuthProv,
      firebase
  }