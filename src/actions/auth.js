import { types } from '../types/types';
import { firebase, googleAuthProv } from '../firebase/firebaseConfig';

export const startLoginEmailPassword = (email, password) => {
    return (dispatch) => {
        setTimeout(() => {
            console.log(`${email} ${password}`)

            dispatch(login(email, password))
        }, 3500);
    }
}

export const startGoogleLogin = () =>{
return ( dispatch) =>{
    firebase.auth().signInWithPopup( googleAuthProv)
    .then( ({user}) => {
       dispatch(
           login( user.uid, user.displayName)
       )
    })
}
}

export const login = (uid, displayName) => {
    return {
        type: types.login,
        payload: {
            uid,
            displayName
        }
    }
}